var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    // This is the "main" file which should include all other modules
    entry: './src/js/main.js',
    // Where should the compiled file go?
    output: {
        // To the `dist` folder
        path: './dist',
        // With the filename `build.js` so it's dist/build.js
        filename: 'script.js'
    },
    module: {
        // Special compilation rules
        loaders: [
            {
                // Ask webpack to check: If this file ends with .js, then apply some transforms
                test: /\.js$/,
                // Transform it with babel
                loader: 'babel',
                // don't transform node_modules folder (which don't need to be compiled)
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css!sass')
            },
            {
                //  Load the Bootstrap Fonts
                test: /\.(ttf|eot|woff2?)(\?[a-z0-9]+)?$/,
                loader: 'url-loader?limit=100000'
            },
            {
                //  Inline SVG Files
                test: /\.svg$/,
                loader: 'svg-inline'
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css', {
            allChunks: true
        })
    ],
    vue: {
        loaders: {
            js: 'babel'
        }
    },
    resolve: {
        alias: {
            'masonry': 'masonry-layout',
            'isotope': 'isotope-layout'
        }
    }
}