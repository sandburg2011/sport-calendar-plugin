<?php
// process.php

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array

    if (empty($_POST['name']))
        $errors['name'] = 'Name is required.';

    if (empty($_POST['email']))
        $errors['email'] = 'Email is required.';

    if (empty($_POST['superheroAlias']))
        $errors['superheroAlias'] = 'Superhero alias is required.';

$data['post'] = $_POST;

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($errors)) {

        // if there are items in our errors array, return those errors
        $data['success'] = false;
        $data['errors']  = $errors;
    } else {

        // if there are no errors process our form, then return a message

        /*global $wpdb;

        $wpdb->insert( wp_events, array(
            'titel' => $_POST['titel'],
            'datetime_start' => $_POST['start'],
            'datetime_end' => $_POST['end'],
            'beschreibung' => $_POST['desc'],
            'ort' => $_POST['place'],
            'preis' => $_POST['price'],
            'link' => $_POST['link'],
            'typ' => $_POST['typ'],
            'city' => $_POST['city'],
        ));

        $last_ai_id = $wpdb->insert_id;

        if (sizeof($_POST['teacher'])!=0) {
            foreach ($_POST['teacher'] as $teacher_id) {
                $wpdb->insert(wp_events_veranstalter_relate, array(
                    'events_id' => $last_ai_id,
                    'veranstalter_id' => $teacher_id,
                ));
            }
        }

        foreach ($_POST['color'] as $color_id){
            $wpdb->insert(wp_events_farben_relate, array(
                'events_id' => $last_ai_id,
                'farbe_id' => $color_id,
            ));
        }*/

        // show a message of success and provide a true success variable
        $data['success'] = true;
        $data['message'] = 'Success!';
    }

    // return all our data to an AJAX call
    echo json_encode($data);






