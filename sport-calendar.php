<?php

/*
Plugin Name: Sport Calendar
Description: Sport Calendar
Version: 1.0
Author: Jannik Schmiedl
Author URI: http://www.jasch-arts.de
License: GPLv2
*/



add_action('admin_menu', 'sport_calendar_plugin_menu');

function sport_calendar_plugin_menu() {

    add_menu_page (
        'Sport Calendar Admin',         //  page title
        'Calendar',                     //  menu title
        'administrator',                //  capability
        'sport-calendar-admin',         //  menu slug
        'render_sport_calendar_plugin', //  callback function
        'dashicons-admin-generic'       //  menu icon
    );

}

function load_plugin_scripts($hook) {

    // Load only on ?page=sport-calendar-admin
    if($hook != 'toplevel_page_sport-calendar-admin') {
        return;
    }
    wp_enqueue_style( 'sport_calendar_style', plugins_url('sport-calendar-plugin/dist/style.css') );

}
add_action( 'admin_enqueue_scripts', 'load_plugin_scripts', 18 );



include 'functions/functions.php';


register_activation_hook( __FILE__, 'database_create' );


function render_sport_calendar_plugin() {

    include('view/app.php');
    
    $custom_script_src = plugins_url('sport-calendar-plugin/dist/script.js');

    ?>
    <script src="<?php echo $custom_script_src ?>"></script>
    <script defer src="https://code.getmdl.io/1.2.1/material.min.js"></script>
    <?php
}

/*
 * Shortcode for Calendar Output
 *
 */

// [event-list type="event"]
function render_event_list_shortcode( $atts ) {
	$a = shortcode_atts( array(
		'type' => 'event',
	), $atts );

	ob_start();
	include('view/shortcode-output.php');
	$shorcode_php_function = ob_get_clean();

	return $shorcode_php_function;
}
add_shortcode( 'event-list', 'render_event_list_shortcode' );