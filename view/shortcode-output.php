<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>

    <?php
    include('output_style.css');
    ?>


</style>

<?php


global $wpdb;

//  Database Table Names defined

$table_prefix = $wpdb->prefix . 'sport_calendar_';

$table_name_events = $table_prefix . 'events';
$table_name_teachers = $table_prefix . 'teachers';
$table_name_places = $table_prefix . 'places';
$table_name_types = $table_prefix . 'types';
$table_name_colors = $table_prefix . 'colors';
$table_name_events_teachers = $table_prefix . 'events_teachers';
$table_name_events_colors = $table_prefix . 'events_colors';

$days = array(
    1 => 'Monday',
    2 => 'Tuesday',
    3 => 'Wednesday',
    4 => 'Thursday',
    5 => 'Friday',
    6 => 'Saturday',
    7 => 'Sunday'
);



/*
 *      EVENTS LIST OUTPUT
 */



//  Get Event Type "Event"
$event_type = $wpdb->get_row("select * from $table_name_types where id=1");

echo "<div id='Event'>";
    echo "<h1>".$event_type->name."</h1>";

    //  Get all Events
    $all_events = $wpdb->get_results("select * from $table_name_events where event_type=1");


    //  Loop through all Events
    foreach($all_events as $result){

        //  Get all Colors for this Event
        $event_colors = $wpdb->get_results("select color from $table_name_events_colors where event=".$result->id);

        //  Get the Events Place
        $event_place = $wpdb->get_results("select icon_url,name from $table_name_places where id=".$result->place);

        //  Get all Teachers for this Event
        $event_teachers = $wpdb->get_results("select teacher from $table_name_events_teachers where event=".$result->id);

        //  Create Date Objects from start and end Dates
        $startDate = date_create($result->startdate);
        $endDate = date_create($result->enddate);

	    $start_time = strftime('%H:%M', strtotime($result->starttime));
	    $end_time = strftime('%H:%M', strtotime($result->endtime));
	    if ($end_time != '00:00'){
		    $time = $start_time.'-'.$end_time.' Uhr';
	    }else{
		    $time = $start_time.' Uhr';
	    }

        //  Put color Values to String
	    $count=0;
	    if (sizeof($event_colors) == 1){
		    foreach ($event_colors as $color) {

			    $colorValue = $wpdb->get_results("select value from $table_name_colors where id=".$color->color);

			    $color_values = $colorValue[0]->value.', '.$colorValue[0]->value;
		    }
	    }else{
		    foreach ($event_colors as $color){

			    $colorValue = $wpdb->get_results("select value from $table_name_colors where id=".$color->color);

			    if ($count==0){
				    $color_values = $colorValue[0]->value;
			    }else{
				    $color_values = $color_values.', '.$colorValue[0]->value;
			    }
			    $count++;
		    }
	    }

	    //  Put Teacher Names and Images to String
	    $count=0;
	    if (sizeof($event_teachers) == 1){
		    foreach ($event_teachers as $event_teacher) {

			    $teacher = $wpdb->get_results("select * from $table_name_teachers where id=".$event_teacher->teacher);

			    $teachers = '<img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
		    }
	    }else{
		    foreach ($event_teachers as $event_teacher) {

			    $teacher = $wpdb->get_results("select * from $table_name_teachers where id=".$event_teacher->teacher);

			    if ($count==0){
				    $teachers = '<img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
			    }else{
				    $teachers = $teachers . ', <img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
			    }
			    $count++;
		    }
	    }




        ?>


        <table class="kalender-table">
            <tr>
			    <?php
			    echo ($event_place[0]->icon_url != '') ? '<div class="kalender-place"><img src="'. $event_place[0]->icon_url .'" alt="'. $event_place[0]->name .'"></div>' : '';
			    ?>
                <td class="table-date">
				    <?php
				    echo '<p class="kalender-monat">' . $startDate->format('F') . '</p>';
				    echo ($startDate->format('d') != $endDate->format('d')) ? '<p class="kalender-tag_range">' . $startDate->format('d') . '-'. $endDate->format('d') . '</p>' : '<p class="kalender-tag">' . $startDate->format('d') . '</p>' ;
				    echo ($startDate->format('F') != $endDate->format('F')) ? '<p class="kalender-monat">' . $endDate->format('F') . '</p>' : '';
				    echo '<p class="kalender-jahr">' . $startDate->format('Y') . '</p>';
				    ?>
                </td>
                <td class="table-desc">
				    <?php
				    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$color_values.');"></div></div><p class="kalender-title">' . $result->title . '</p></div>';
				    echo $teachers;
				    echo '<p>';
				    echo '<span class="kalender-text"><i class="material-icons">place</i>' . $result->address . '</span>';
				    echo '<span class="kalender-text"><i class="material-icons">access_time</i>' . $time . '</span>';
				    echo ($result->price != '') ? '<span class="kalender-text"><i class="material-icons">euro_symbol</i>' . $result->price . '</span>' : '';
				    echo '</p>';
				    echo '<p class="kalender-text">' . $result->description . '</p>';
				    echo ($result->url != null) ? '<a class="kalender-text" href="'.$result->url.'" target="_blank">[mehr]</a>' : '' ;
				    ?>
                </td>
            </tr>
        </table>


        <?php
    }
echo "</div>";



/*
 *      CLASSES LIST OUTPUT
 */
  
  
  //  Get Event Type "Event"
$event_type = $wpdb->get_row("select * from $table_name_types where id=2");

echo "<div id='Class'>";
    echo "<h1>".$event_type->name."</h1>";

    //  Get all Classes
    $all_classes = $wpdb->get_results("select * from $table_name_events where event_type=2");


    //  Loop through all Events
    foreach($all_classes as $resultClasses){

        //  Get all Colors for this Event
        $event_colors = $wpdb->get_results("select color from $table_name_events_colors where event=".$resultClasses->id);

        //  Get the Events Place
        $event_place = $wpdb->get_results("select icon_url,name from $table_name_places where id=".$resultClasses->place);

        //  Get all Teachers for this Event
        $event_teachers = $wpdb->get_results("select teacher from $table_name_events_teachers where event=".$resultClasses->id);


        //Create Date Objects from start and end Dates
        $startDate = date_create($resultClasses->startdate);
        
        $start_time = strftime('%H:%M', strtotime($resultClasses->starttime));
	    $end_time = strftime('%H:%M', strtotime($resultClasses->endtime));
	    
	    if ($end_time != '00:00'){
		    $time = $start_time.'-'.$end_time.' Uhr';
	    }else{
		    $time = $start_time.' Uhr';
	    }

        //  Put color Values to String
	    $count=0;
	    if (sizeof($event_colors) == 1){
		    foreach ($event_colors as $color) {

			    $colorValue = $wpdb->get_results("select value from $table_name_colors where id=".$color->color);

			    $color_values = $colorValue[0]->value.', '.$colorValue[0]->value;
		    }
	    }else{
		    foreach ($event_colors as $color){

			    $colorValue = $wpdb->get_results("select value from $table_name_colors where id=".$color->color);

			    if ($count==0){
				    $color_values = $colorValue[0]->value;
			    }else{
				    $color_values = $color_values.', '.$colorValue[0]->value;
			    }
			    $count++;
		    }
	    }

	    //  Put Teacher Names and Images to String
	    $count=0;
	    if (sizeof($event_teachers) == 1){
		    foreach ($event_teachers as $event_teacher) {

			    $teacher = $wpdb->get_results("select * from $table_name_teachers where id=".$event_teacher->teacher);

			    $teachers = '<img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
		    }
	    }else{
		    foreach ($event_teachers as $event_teacher) {

			    $teacher = $wpdb->get_results("select * from $table_name_teachers where id=".$event_teacher->teacher);

			    if ($count==0){
				    $teachers = '<img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
			    }else{
				    $teachers = $teachers . ', <img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
			    }
			    $count++;
		    }
	    }




        ?>


        <table class="kalender-table">
            <tr>
			    <?php
			    echo ($event_place[0]->icon_url != '') ? '<div class="kalender-place"><img src="'. $event_place[0]->icon_url .'" alt="'. $event_place[0]->name .'"></div>' : '';
			    ?>
                <td class="table-date">
				    <?php
				    echo '<p class="kalender-weekday">'.$days[$resultClasses->day_of_week].'</p>';
				    echo '<p class="kalender-jahr">' . $startDate->format('Y') . '</p>';
				    ?>
                </td>
                <td class="table-desc">
				    <?php
				    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$color_values.');"></div></div><p class="kalender-title">' . $resultClasses->title . '</p></div>';
				    echo $teachers;
				    echo '<p>';
				    echo '<span class="kalender-text"><i class="material-icons">place</i>' . $resultClasses->address . '</span>';
				    echo '<span class="kalender-text"><i class="material-icons">access_time</i>' . $time . '</span>';
				    echo ($result->price != '') ? '<span class="kalender-text"><i class="material-icons">euro_symbol</i>' . $resultClasses->price . '</span>' : '';
				    echo '</p>';
				    echo '<p class="kalender-text">' . $resultClasses->description . '</p>';
				    echo ($resultClasses->url != null) ? '<a class="kalender-text" href="'.$resultClasses->url.'" target="_blank">[mehr]</a>' : '' ;
				    ?>
                </td>
            </tr>
        </table>


        <?php
    }
echo "</div>";





/*
 *      Jams LIST OUTPUT
 */
  
  
  //  Get Event Type "Event"
$event_type = $wpdb->get_row("select * from $table_name_types where id=3");

echo "<div id='Class'>";
    echo "<h1>".$event_type->name."</h1>";

    //  Get all Classes
    $all_jams = $wpdb->get_results("select * from $table_name_events where event_type=3");


    //  Loop through all Events
    foreach($all_jams as $resultJams){

        //  Get all Colors for this Event
        $event_colors = $wpdb->get_results("select color from $table_name_events_colors where event=".$resultJams->id);

        //  Get the Events Place
        $event_place = $wpdb->get_results("select icon_url,name from $table_name_places where id=".$resultJams->place);

        //  Get all Teachers for this Event
        $event_teachers = $wpdb->get_results("select teacher from $table_name_events_teachers where event=".$resultJams->id);


        //Create Date Objects from start and end Dates
        $startDate = date_create($resultJams->startdate);
        
        $start_time = strftime('%H:%M', strtotime($resultJams->starttime));
	    $end_time = strftime('%H:%M', strtotime($resultJams->endtime));
	    
	    if ($end_time != '00:00'){
		    $time = $start_time.'-'.$end_time.' Uhr';
	    }else{
		    $time = $start_time.' Uhr';
	    }

        //  Put color Values to String
	    $count=0;
	    if (sizeof($event_colors) == 1){
		    foreach ($event_colors as $color) {

			    $colorValue = $wpdb->get_results("select value from $table_name_colors where id=".$color->color);

			    $color_values = $colorValue[0]->value.', '.$colorValue[0]->value;
		    }
	    }else{
		    foreach ($event_colors as $color){

			    $colorValue = $wpdb->get_results("select value from $table_name_colors where id=".$color->color);

			    if ($count==0){
				    $color_values = $colorValue[0]->value;
			    }else{
				    $color_values = $color_values.', '.$colorValue[0]->value;
			    }
			    $count++;
		    }
	    }

	    //  Put Teacher Names and Images to String
	    $count=0;
	    if (sizeof($event_teachers) == 1){
		    foreach ($event_teachers as $event_teacher) {

			    $teacher = $wpdb->get_results("select * from $table_name_teachers where id=".$event_teacher->teacher);

			    $teachers = '<img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
		    }
	    }else{
		    foreach ($event_teachers as $event_teacher) {

			    $teacher = $wpdb->get_results("select * from $table_name_teachers where id=".$event_teacher->teacher);

			    if ($count==0){
				    $teachers = '<img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
			    }else{
				    $teachers = $teachers . ', <img src="'.$teacher[0]->avatar_url.'" class="lehrer-img"><p class="lehrer-name">' . $teacher[0]->name . ' ' . $teacher[0]->lastname . '</p>';
			    }
			    $count++;
		    }
	    }




        ?>


        <table class="kalender-table">
            <tr>
			    <?php
			    echo ($event_place[0]->icon_url != '') ? '<div class="kalender-place"><img src="'. $event_place[0]->icon_url .'" alt="'. $event_place[0]->name .'"></div>' : '';
			    ?>
                <td class="table-date">
				    <?php
				    echo '<p class="kalender-weekday">'.$days[$resultJams->day_of_week].'</p>';
				    echo '<p class="kalender-jahr">' . $startDate->format('Y') . '</p>';
				    ?>
                </td>
                <td class="table-desc">
				    <?php
				    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$color_values.');"></div></div><p class="kalender-title">' . $resultJams->title . '</p></div>';
				    echo $teachers;
				    echo '<p>';
				    echo '<span class="kalender-text"><i class="material-icons">place</i>' . $resultJams->address . '</span>';
				    echo '<span class="kalender-text"><i class="material-icons">access_time</i>' . $time . '</span>';
				    echo ($result->price != '') ? '<span class="kalender-text"><i class="material-icons">euro_symbol</i>' . $resultJams->price . '</span>' : '';
				    echo '</p>';
				    echo '<p class="kalender-text">' . $resultJams->description . '</p>';
				    echo ($resultJams->url != null) ? '<a class="kalender-text" href="'.$resultJams->url.'" target="_blank">[mehr]</a>' : '' ;
				    ?>
                </td>
            </tr>
        </table>


        <?php
    }
echo "</div>";
echo "</table>";
echo "<br/>";
?>

