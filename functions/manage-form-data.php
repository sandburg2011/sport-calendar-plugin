<?php

//  Get Initial Data

add_action('wp_ajax_get_events', 'get_events');

function get_events(){
	global $wpdb;

	$table_prefix = $wpdb->prefix . 'sport_calendar_';
	$table_name_events = $table_prefix . 'events';

	$table_name_events_colors = $table_prefix . 'events_colors';
	$table_name_events_teachers = $table_prefix . 'events_teachers';
	$table_name_colors = $table_prefix . 'colors';
	$table_name_teachers = $table_prefix . 'teachers';


	$events = $wpdb->get_results(
		"SELECT
				$table_name_events.*,
				GROUP_CONCAT(DISTINCT $table_name_colors.id) AS colors,
				GROUP_CONCAT(DISTINCT $table_name_teachers.id) AS teachers
			FROM $table_name_events
			JOIN $table_name_events_colors ON $table_name_events.id = $table_name_events_colors.event
			JOIN $table_name_colors ON $table_name_colors.id = $table_name_events_colors.color
			JOIN $table_name_events_teachers ON $table_name_events.id = $table_name_events_teachers.event
			JOIN $table_name_teachers ON $table_name_teachers.id = $table_name_events_teachers.teacher
			GROUP BY $table_name_events.id
			ORDER BY $table_name_events.startdate"
	);

	echo json_encode($events);

	wp_die();
}

add_action('wp_ajax_get_colors', 'get_colors');

function get_colors(){
    global $wpdb;

    $table_prefix = $wpdb->prefix . 'sport_calendar_';
    $table_name_colors = $table_prefix . 'colors';

    $colors = $wpdb->get_results(
        "SELECT * FROM $table_name_colors"
    );

    echo json_encode($colors);

    wp_die();
}

add_action('wp_ajax_get_places', 'get_places');

function get_places(){
    global $wpdb;

    $table_prefix = $wpdb->prefix . 'sport_calendar_';
    $table_name_places = $table_prefix . 'places';

    $places = $wpdb->get_results(
        "SELECT * FROM $table_name_places"
    );


    echo json_encode($places);

    wp_die();
}

add_action('wp_ajax_get_teachers', 'get_teachers');

function get_teachers(){
    global $wpdb;

    $table_prefix = $wpdb->prefix . 'sport_calendar_';
    $table_name_teachers = $table_prefix . 'teachers';

    $teachers = $wpdb->get_results(
        "SELECT * FROM $table_name_teachers"
    );

    echo json_encode($teachers);

    wp_die();
}

function is_initialized(){
    global $wpdb;

    $table_prefix = $wpdb->prefix . 'sport_calendar_';
    $table_name_config = $table_prefix . 'config';

    $initialized = $wpdb->get_results(
        "SELECT * FROM $table_name_config WHERE name = 'is_initialized'"
    );

    return $initialized;

    wp_die();
}

add_action('wp_ajax_delete_event', 'delete_event');

function delete_event(){

    global $wpdb;
    $table_prefix = $wpdb->prefix . 'sport_calendar_';
    $table_name_events = $table_prefix . 'events';
    $table_name_events_colors = $table_prefix . 'events_colors';
    $table_name_events_teachers = $table_prefix . 'events_teachers';

    $event_id = $_POST['id'];

    $wpdb->delete($table_name_events_colors, array('event' => $event_id));
    $wpdb->delete($table_name_events_teachers, array('event' => $event_id));

    $success = $wpdb->delete($table_name_events, array('id' => $event_id));

    echo json_encode($success);
    wp_die();

}


add_action('wp_ajax_event_form_submit', 'event_form_submit');

function event_form_submit(){

	global $wpdb;
	$table_prefix = $wpdb->prefix . 'sport_calendar_';

	$form_data = $_POST;

	$teachers = json_decode($form_data['teachers']);
	$colors = json_decode($form_data['colors']);


	//  remove Values from form_data array which not events table params
	unset($form_data['action']);
	unset($form_data['teachers']);
	unset($form_data['colors']);

	$table_name_events = $table_prefix . 'events';
	$table_name_events_colors = $table_prefix . 'events_colors';
	$table_name_events_teachers = $table_prefix . 'events_teachers';

	//  if new event insert
	if(!isset($form_data['id'])){

		$success = $wpdb->insert($table_name_events, $form_data);
		$event_id = $wpdb->insert_id;

		if($success){

			foreach($colors as $color){
				$wpdb->insert($table_name_events_colors, array(
					'event' => $event_id,
					'color' => $color,
				));
			}

			foreach($teachers as $teacher){
				$wpdb->insert($table_name_events_teachers, array(
					'event' => $event_id,
					'teacher' => $teacher,
				));
			}
		}
	}else{

		//  Update Event

		$event_id = $form_data['id'];
		unset($form_data['id']);

		$success = $wpdb->update($table_name_events, $form_data, array('id' => $event_id));

		if($success !== false){


			$wpdb->delete($table_name_events_colors, array('event' => $event_id));
			$wpdb->delete($table_name_events_teachers, array('event' => $event_id));

			foreach($colors as $color){
				$wpdb->insert($table_name_events_colors, array(
					'event' => $event_id,
					'color' => $color,
				));
			}

			foreach($teachers as $teacher){
				$wpdb->insert($table_name_events_teachers, array(
					'event' => $event_id,
					'teacher' => $teacher,
				));
			}
		}



	}


    echo json_encode($success);

    wp_die();
}

add_action('wp_ajax_init_plugin_database', 'init_plugin_database');

function init_plugin_database(){

    global $wpdb;
    $table_prefix = $wpdb->prefix . 'sport_calendar_';

    $form_data = $_POST;

    $table_name_colors = $table_prefix . 'colors';
    for ($i = 0 ; $i < $form_data['colors']; $i++){

    	if(isset($form_data['color-'.$i.'-id'])){
		    $wpdb->update($table_name_colors,
		    array(
			    'value' => $form_data['color-'.$i.'-value']
		    ),
		    array(
		    	'id' => $form_data['color-'.$i.'-id']
		    ));
	    } else {
		    $wpdb->insert($table_name_colors, array(
			    'value' => $form_data['color-'.$i.'-value']
		    ));
	    }

    }

    $table_name_places = $table_prefix . 'places';
    for ($i = 0 ; $i < $form_data['places']; $i++){

	    if(isset($form_data['place-'.$i.'-id'])){


	    	if(isset($_FILES['place-'.$i.'-icon'])){

			    $icon_id = media_handle_upload( 'place-' . $i . '-icon', 0 );

			    $wpdb->update($table_name_places,
				    array(
					    'name' => $form_data[ 'place-' . $i . '-name' ],
					    'icon_url' => wp_get_attachment_url( $icon_id )
				    ),
				    array(
					    'id' => $form_data['place-'.$i.'-id']
				    )
			    );
		    } else {

			    $wpdb->update($table_name_places,
				    array(
					    'name' => $form_data[ 'place-' . $i . '-name' ]
				    ),
				    array(
					    'id' => $form_data['place-'.$i.'-id']
				    )
			    );
		    }

	    } else {

		    $icon_id = media_handle_upload( 'place-' . $i . '-icon', 0 );

		    $wpdb->insert( $table_name_places, array(
			    'name'     => $form_data[ 'place-' . $i . '-name' ],
			    'icon_url' => wp_get_attachment_url( $icon_id )
		    ) );

	    }
    }


    $table_name_teachers = $table_prefix . 'teachers';
    for ($i = 0 ; $i < $form_data['teachers']; $i++){

	    if(isset($form_data['teacher-'.$i.'-id'])){

		    if(isset($_FILES['teacher-'.$i.'-avatar'])){

			    $avatar_id = media_handle_upload('teacher-'.$i.'-avatar', 0);

			    $wpdb->update($table_name_teachers,
				    array(
					    'name' => $form_data['teacher-'.$i.'-name'],
					    'lastname' => $form_data['teacher-'.$i.'-lastname'],
					    'avatar_url' => wp_get_attachment_url($avatar_id)
				    ),
				    array(
					    'id' => $form_data['teacher-'.$i.'-id']
				    )
			    );
		    } else {
			    $wpdb->update($table_name_teachers,
				    array(
					    'name' => $form_data['teacher-'.$i.'-name'],
					    'lastname' => $form_data['teacher-'.$i.'-lastname']
				    ),
				    array(
					    'id' => $form_data['teacher-'.$i.'-id']
				    )
			    );
		    }

	    } else {

		    $avatar_id = media_handle_upload('teacher-'.$i.'-avatar', 0);

		    $wpdb->insert($table_name_teachers, array(
			    'name' => $form_data['teacher-'.$i.'-name'],
			    'lastname' => $form_data['teacher-'.$i.'-lastname'],
			    'avatar_url' => wp_get_attachment_url($avatar_id)
		    ));

	    }

    }


    //  Event Config 'is_initialized' predefined
    $table_name_config = $table_prefix . 'config';

    $wpdb->update(
        $table_name_config,
        array(
            'value' => 1
        ),
        array(
            'name' => 'is_initialized'
        )
    );


    echo json_encode($form_data);

    wp_die();
}