<?php

use \WeDevs\ORM\Eloquent\Facades\DB;

function database_create() {

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	global $wpdb;

	$table_prefix = $wpdb->prefix . 'sport_calendar_';
	$charset_collate = $wpdb->get_charset_collate();

    $table_name_config = $table_prefix . 'config';
    $sql_config = "CREATE TABLE $table_name_config (
    	id int(10) unsigned NOT NULL AUTO_INCREMENT,
    	name varchar(80) DEFAULT NULL,
    	value tinyint(1) DEFAULT 0,
    	PRIMARY KEY (id)
	) $charset_collate;";
    dbDelta( $sql_config );


	$table_name_colors = $table_prefix . 'colors';
	$sql_colors = "CREATE TABLE $table_name_colors (
    	id int(10) unsigned NOT NULL AUTO_INCREMENT,
    	value varchar(80) DEFAULT NULL,
    	PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_colors );

	$table_name_places = $table_prefix . 'places';
	$sql_places = "CREATE TABLE $table_name_places (
	  	id int(10) unsigned NOT NULL AUTO_INCREMENT,
	 	name varchar(80) DEFAULT NULL,
	 	icon_url varchar(200) DEFAULT NULL,
	  	PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_places );

	$table_name_teachers = $table_prefix . 'teachers';
	$sql_teachers = "CREATE TABLE $table_name_teachers (
  		id int(10) unsigned NOT NULL AUTO_INCREMENT,
		name varchar(80) DEFAULT NULL,
		lastname varchar(80) DEFAULT NULL,
		avatar_url varchar(200) DEFAULT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_teachers );

	$table_name_types = $table_prefix . 'types';
	$sql_types = "CREATE TABLE $table_name_types (
		id int(10) unsigned NOT NULL AUTO_INCREMENT,
		name varchar(80) DEFAULT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_types );

	$table_name_events = $table_prefix . 'events';
	$sql_events = "CREATE TABLE $table_name_events (
	  	id int(10) unsigned NOT NULL AUTO_INCREMENT,
	  	title varchar(80) DEFAULT NULL,
	  	description varchar(200) DEFAULT NULL,
	  	startdate date DEFAULT NULL,
	  	enddate date DEFAULT NULL,
	  	starttime time DEFAULT NULL,
	  	endtime time DEFAULT NULL,
		day_of_week int(2) DEFAULT NULL,
	  	address varchar(100) DEFAULT NULL,
	  	price varchar(80) DEFAULT NULL,
	  	event_type int(10) unsigned DEFAULT NULL,
	  	place int(10) unsigned DEFAULT NULL,
	  	url varchar(200) DEFAULT NULL,
	  	PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_events );

	$sql_events_types_foreign = "ALTER TABLE " . $table_name_events . " ADD FOREIGN KEY (event_type) REFERENCES " . $table_name_types . "(id);";
	$wpdb->query($sql_events_types_foreign);
	$sql_events_places_foreign = "ALTER TABLE " . $table_name_events . " ADD FOREIGN KEY (place) REFERENCES " . $table_name_places . "(id);";
	$wpdb->query($sql_events_places_foreign);


	$table_name_events_colors = $table_prefix . 'events_colors';
	$sql_events_colors = "CREATE TABLE $table_name_events_colors (
		id int(10) unsigned NOT NULL AUTO_INCREMENT,
		event int(10) unsigned DEFAULT NULL,
		color int(10) unsigned DEFAULT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_events_colors );

	$sql_events_colors_event_foreign = "ALTER TABLE " . $table_name_events_colors . " ADD FOREIGN KEY (event) REFERENCES " . $table_name_events . "(id);";
	$wpdb->query($sql_events_colors_event_foreign);
	$sql_events_colors_color_foreign = "ALTER TABLE " . $table_name_events_colors . " ADD FOREIGN KEY (color) REFERENCES " . $table_name_colors . "(id);";
	$wpdb->query($sql_events_colors_color_foreign);


	$table_name_events_teachers = $table_prefix . 'events_teachers';
	$sql_events_teachers = "CREATE TABLE $table_name_events_teachers (
		id int(10) unsigned NOT NULL AUTO_INCREMENT,
		event int(10) unsigned DEFAULT NULL,
		teacher int(10) unsigned DEFAULT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";
	dbDelta( $sql_events_teachers );

	$sql_events_teachers_event_foreign = "ALTER TABLE " . $table_name_events_teachers . " ADD FOREIGN KEY (event) REFERENCES " . $table_name_events . "(id);";
	$wpdb->query($sql_events_teachers_event_foreign);
	$sql_events_teachers_teacher_foreign = "ALTER TABLE " . $table_name_events_teachers . " ADD FOREIGN KEY (teacher) REFERENCES " . $table_name_teachers . "(id);";
	$wpdb->query($sql_events_teachers_teacher_foreign);


	create_initial_data();

}

function create_initial_data() {

	global $wpdb;
	$table_prefix = $wpdb->prefix . 'sport_calendar_';


	//  Event Types predefined
	$table_name_types = $table_prefix . 'types';

	$type_1_name = 'Event';
	$type_2_name = 'Class';
	$type_3_name = 'Jam';

	$wpdb->insert($table_name_types, array('name' => $type_1_name));
	$wpdb->insert($table_name_types, array('name' => $type_2_name));
	$wpdb->insert($table_name_types, array('name' => $type_3_name));


	//  Event Config 'is_initialized' predefined
	$table_name_config = $table_prefix . 'config';

	$wpdb->insert($table_name_config, array(
		'name' => 'is_initialized',
		'value' => 0
	));
}