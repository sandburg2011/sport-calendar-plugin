<?php get_header(); ?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    $image_src = wp_get_attachment_image_src( get_post_thumbnail_id(),'full' );
    echo '<div class="header-background headerSizePage hidden-xs" style="background: url(' . $image_src[0]  . ') no-repeat center center;"></div>';
    echo '<div class="header-overlay headerSizePage hidden-xs"></div>';
    ?>
<?php endwhile; ?><?php endif; ?>

<?php get_sidebar(); ?>

    <div class="container">

        <h1><?php the_title(); ?><br /><img src="<?php bloginfo('template_url'); ?>/assets/img/trenner.gif" class="h1-trenner"></h1>

        <div class="events-breadcrump text-center text-uppercase"><a href="#upcoming">Veranstaltungen</a><a href="#classes">Kurse</a><a href="#jams">Jams</a><a href="#orte">Orte</a></div>

        <div class="row">
            <div id="upcoming" class="col-xs-12 col-md-10 col-md-offset-1">
                <div id="upcoming-row">

                <h2>Bevorstehende Veranstaltungen</h2>
                <?php

                $image_url = get_template_directory_uri().'/assets/img/staedte/';

                global $wpdb;

                $query = get_upcoming_query();

                $count_query = get_count_query();

                $total = $wpdb->get_var( $count_query );
                $items_per_page = 7;
                $page = isset( $_GET['events'] ) ? abs( (int) $_GET['events'] ) : 1;
                $offset = ( $page * $items_per_page ) - $items_per_page;
                $events = $wpdb->get_results( $query . " LIMIT ${offset}, ${items_per_page}" );

                if (sizeof($events) != 0 ) {
                    foreach ($events as $event) {


                        setlocale(LC_TIME, 'de_DE@euro', 'de_DE', 'de', 'ge');

                        $start = $event->datetime_start;
                        $end = $event->datetime_end;
                        $title = $event->titel;
                        $location = $event->ort;
                        $city = $event->city;
                        $description = $event->beschreibung;
                        $color = $event->wert;
                        $link = $event->link;
                        $start_time = strftime('%H:%M', strtotime($event->datetime_start));
                        $end_time = strftime('%H:%M', strtotime($event->datetime_end));
                        if ($end_time != '00:00'){
                            $time = $start_time.'-'.$end_time.' Uhr';
                        }else{
                            $time = $start_time.' Uhr';
                        }
                        $price = $event->preis;

                        $events_id = $event->events_id;
                        $lehrerarray = array();
                        $lehrer_namen = null;
                        $lehrerarray = $wpdb->get_results(get_teacher_query() . " WHERE ${events_id} = wp_events_veranstalter_relate.events_id" );
                        $farbenarray = $wpdb->get_results(get_color_query() . " WHERE ${events_id} = wp_events_farben_relate.events_id" );

                        $count=0;
                        if (sizeof($lehrerarray) == 1){
                            foreach ($lehrerarray as $lehrer) {
                                $lehrerurl = get_site_url().'/leute/'.strtolower(str_replace(" ", "-", $lehrer->name));
                                $lehrer_namen = '<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                            }
                        }else{
                            foreach ($lehrerarray as $lehrer){
                                $lehrerurl = get_site_url().'/leute/'.strtolower(str_replace(" ", "-", $lehrer->name));
                                if ($count==0){
                                    $lehrer_namen = '<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                                }else{
                                    $lehrer_namen = $lehrer_namen.', '.'<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                                }
                                $count++;
                            }
                        }

                        $farbenarray = $wpdb->get_results(get_color_query() . " WHERE ${events_id} = wp_events_farben_relate.events_id" );

                        $count=0;
                        if (sizeof($farbenarray) == 1){
                            foreach ($farbenarray as $farbe) {
                                $farben_werte = $farbe->wert.', '.$farbe->wert;
                            }
                        }else{
                            foreach ($farbenarray as $farbe){
                                if ($count==0){
                                    $farben_werte = $farbe->wert;
                                }else{
                                    $farben_werte = $farben_werte.', '.$farbe->wert;
                                }
                                $count++;
                            }
                        }

                        ?>
                        <table class="kalender-table">
                            <tr>
                                <?php
                                echo ($city != 'hh' && $city != '') ? '<div class="kalender-place"><img src="'. $image_url .''. $city .'.png" alt="'. $city .'"></div>' : '';
                                ?>
                                <td class="table-date">
                                    <?php
                                    echo '<p class="kalender-monat">' . iconv('ISO-8859-1', 'UTF-8', strftime('%B', strtotime($start))) . '</p>';
                                    echo (strftime('%d', strtotime($start))!=(strftime('%d', strtotime($end)))) ? '<p class="kalender-tag_range">' . strftime('%d', strtotime($start)) . '-'. strftime('%d', strtotime($end)) . '</p>' : '<p class="kalender-tag">' . strftime('%d', strtotime($start)) . '</p>' ;
                                    echo (strftime('%B', strtotime($end)) != strftime('%B', strtotime($start))) ? '<p class="kalender-monat">' . iconv('ISO-8859-1', 'UTF-8', strftime('%B', strtotime($end))) . '</p>' : '';
                                    echo '<p class="kalender-jahr">' . strftime('%Y', strtotime($start)) . '</p>';
                                    ?>
                                </td>
                                <td class="table-desc">
                                    <?php
                                    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$farben_werte.');"></div></div><p class="kalender-title">' . $title . '</p></div>';
                                    echo ($lehrer_namen != null) ? '<p class="kalender-text">Mit: ' . $lehrer_namen . '</p>' : '';
                                    echo '<p>';
                                    echo ($location != null) ? '<span class="kalender-text"><span class="typcn typcn-location"></span>' . $location . '</span>' : '';
                                    echo ($time != null) ? '<span class="kalender-text"><span class="typcn typcn-time"></span>' . $time . '</span>' : '';
                                    echo ($price != null) ? '<span class="kalender-text"><span class="glyphicon glyphicon-euro"></span>' . $price . '</span>' : '';
                                    echo '</p>';
                                    echo '<span class="kalender-text">' . $description . ' ';
                                    echo ($link != null) ? '<a class="kalender-text" href="'.$link.'" target="_blank">[mehr]</a></span>' : '' ;
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                } else {
                    echo '<p>Es gibt in der nächsten Zeit keine Veranstaltungen.</p>';
                }

                echo '<div class="events-pagination text-center">';
                echo paginate_links( array(
                    'base' => add_query_arg( 'events', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => ceil($total / $items_per_page),
                    'current' => $page
                ));
                echo '</div>';
                ?>
            </div>
            </div>

            <div id="past" class="col-xs-12 col-md-10 col-md-offset-1">
                <div id="past-row">

                <h2>Das hast du hoffentlich nicht verpasst</h2>
                <?php

                $past_query = get_past_query();

                $count_past_query = get_count_past_query();

                $total_past = $wpdb->get_var( $count_past_query );
                $items_per_past_page = 2;
                $past_page = isset( $_GET['pastevents'] ) ? abs( (int) $_GET['pastevents'] ) : 1;
                $past_offset = ( $past_page * $items_per_past_page ) - $items_per_past_page;
                $past_events = $wpdb->get_results( $past_query . " LIMIT ${past_offset}, ${items_per_past_page}" );

                if (sizeof($past_events) != 0 ) {
                    foreach ($past_events as $event) {


                        setlocale(LC_TIME, 'de_DE@euro', 'de_DE', 'de', 'ge');

                        $start = $event->datetime_start;
                        $end = $event->datetime_end;
                        $title = $event->titel;
                        $location = $event->ort;
                        $city = $event->city;
                        $description = $event->beschreibung;
                        $link = $event->link;
                        $start_time = strftime('%H:%M', strtotime($event->datetime_start));
                        $end_time = strftime('%H:%M', strtotime($event->datetime_end));
                        if ($end_time != '00:00'){
                            $time = $start_time.'-'.$end_time.' Uhr';
                        }else{
                            $time = $start_time.' Uhr';
                        }
                        $price = $event->preis;

                        $events_id = $event->events_id;
                        $lehrerarray = array();
                        $lehrer_namen = null;
                        $lehrerarray = $wpdb->get_results(get_teacher_query() . " WHERE ${events_id} = wp_events_veranstalter_relate.events_id" );

                        $count=0;
                        if (sizeof($lehrerarray) == 1){
                            foreach ($lehrerarray as $lehrer) {
                                $lehrerurl = get_site_url().'/leute/'.strtolower(str_replace(" ", "-", $lehrer->name));
                                $lehrer_namen = '<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                            }
                        }else{
                            foreach ($lehrerarray as $lehrer){
                                $lehrerurl = get_site_url().'/leute/'.strtolower(str_replace(" ", "-", $lehrer->name));
                                if ($count==0){
                                    $lehrer_namen = '<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                                }else{
                                    $lehrer_namen = $lehrer_namen.', '.'<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                                }
                                $count++;
                            }
                        }

                        $farbenarray = $wpdb->get_results(get_color_query() . " WHERE ${events_id} = wp_events_farben_relate.events_id" );

                        $count=0;
                        if (sizeof($farbenarray) == 1){
                            foreach ($farbenarray as $farbe) {
                                $farben_werte = $farbe->wert.', '.$farbe->wert;
                            }
                        }else{
                            foreach ($farbenarray as $farbe){
                                if ($count==0){
                                    $farben_werte = $farbe->wert;
                                }else{
                                    $farben_werte = $farben_werte.', '.$farbe->wert;
                                }
                                $count++;
                            }
                        }

                        ?>
                        <table class="kalender-table">
                            <tr>
                                <?php
                                echo ($city != 'hh' && $city != '') ? '<div class="kalender-place"><img src="'. $image_url .''. $city .'.png" alt="'. $city .'"></div>' : '';
                                ?>
                                <td class="table-date">
                                    <?php
                                    echo '<p class="kalender-monat">' . iconv('ISO-8859-1', 'UTF-8', strftime('%B', strtotime($start))) . '</p>';
                                    echo (strftime('%d', strtotime($start))!=(strftime('%d', strtotime($end)))) ? '<p class="kalender-tag_range">' . strftime('%d', strtotime($start)) . '-'. strftime('%d', strtotime($end)) . '</p>' : '<p class="kalender-tag">' . strftime('%d', strtotime($start)) . '</p>' ;
                                    echo (strftime('%B', strtotime($end)) != strftime('%B', strtotime($start))) ? '<p class="kalender-monat">' . iconv('ISO-8859-1', 'UTF-8', strftime('%B', strtotime($end))) . '</p>' : '';
                                    echo '<p class="kalender-jahr">' . strftime('%Y', strtotime($start)) . '</p>';
                                    ?>
                                </td>
                                <td class="table-desc">
                                    <?php
                                    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$farben_werte.');"></div></div><p class="kalender-title">' . $title . '</p></div>';
                                    echo ($lehrer_namen != null) ? '<p class="kalender-text">Mit: ' . $lehrer_namen . '</p>' : '';
                                    echo '<p>';
                                    echo ($location != null) ? '<span class="kalender-text"><span class="typcn typcn-location"></span>' . $location . '</span>' : '';
                                    echo ($time != null) ? '<span class="kalender-text"><span class="typcn typcn-time"></span>' . $time . '</span>' : '';
                                    echo ($price != null) ? '<span class="kalender-text"><span class="glyphicon glyphicon-euro"></span>' . $price . '</span>' : '';
                                    echo '</p>';
                                    echo '<span class="kalender-text">' . $description . ' ';
                                    echo ($link != null) ? '<a class="kalender-text" href="'.$link.'" target="_blank">[mehr]</a></span>' : '' ;
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                }else {
                    echo '<p>Du hast in der letzten Zeit zum Glück nichts verpasst.</p>';
                }

                echo '<div class="past-events-pagination text-center">';
                echo paginate_links( array(
                    'base' => add_query_arg( 'pastevents', '%#%'),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => ceil($total_past / $items_per_past_page),
                    'current' => $past_page
                ));
                echo '</div>';

                ?>
            </div>
            </div>

            <div id="classes" class="col-xs-12 col-md-10 col-md-offset-1">

                <h2>Kurse</h2>
                <?php


                if (sizeof(get_events_classes()) != 0) {
                    foreach (get_events_classes() as $event) {

                        setlocale(LC_TIME, 'de_DE@euro', 'de_DE', 'de', 'ge');


                        $weekday = strftime('%a', strtotime($event->datetime_start));
                        $start_time = strftime('%H:%M', strtotime($event->datetime_start));
                        $end_time = strftime('%H:%M', strtotime($event->datetime_end));
                        $title = $event->titel;
                        $description = $event->beschreibung;
                        $location = $event->ort;
                        $city = $event->city;
                        $price = $event->preis;

                        $events_id = $event->events_id;
                        $lehrerarray = array();
                        $lehrer_namen = null;
                        $lehrerarray = $wpdb->get_results(get_teacher_query() . " WHERE ${events_id} = wp_events_veranstalter_relate.events_id" );

                        $count=0;
                        if (sizeof($lehrerarray) == 1){
                            foreach ($lehrerarray as $lehrer) {
                                $lehrerurl = get_site_url().'/leute/'.strtolower(str_replace(" ", "-", $lehrer->name));
                                $lehrer_namen = '<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                            }
                        }else{
                            foreach ($lehrerarray as $lehrer){
                                $lehrerurl = get_site_url().'/leute/'.strtolower(str_replace(" ", "-", $lehrer->name));
                                if ($count==0){
                                    $lehrer_namen = '<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                                }else{
                                    $lehrer_namen = $lehrer_namen.', '.'<a href="'.$lehrerurl.'">'.$lehrer->name.'</a>';
                                }
                                $count++;
                            }
                        }

                        $farbenarray = $wpdb->get_results(get_color_query() . " WHERE ${events_id} = wp_events_farben_relate.events_id" );

                        $count=0;
                        if (sizeof($farbenarray) == 1){
                            foreach ($farbenarray as $farbe) {
                                $farben_werte = $farbe->wert.', '.$farbe->wert;
                            }
                        }else{
                            foreach ($farbenarray as $farbe){
                                if ($count==0){
                                    $farben_werte = $farbe->wert;
                                }else{
                                    $farben_werte = $farben_werte.', '.$farbe->wert;
                                }
                                $count++;
                            }
                        }


                        ?>

                        <table class="kalender-table">
                            <tr>
                                <?php
                                echo ($city != 'hh' && $city != '') ? '<div class="kalender-place"><img src="'. $image_url .''. $city .'.png" alt="'. $city .'"></div>' : '';
                                ?>
                                <td class="table-date">
                                    <?php
                                    echo '<p class="kalender-tag">' . $weekday . '</p>';
                                    echo '<p class="kalender-jahr">' . $start_time . '</p>';
                                    echo '<p class="kalender-jahr">' . $end_time . '</p>';
                                    ?>
                                </td>
                                <td class="table-desc">
                                    <?php
                                    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$farben_werte.');"></div></div><span class="kalender-title">' . $title . '</span>';
                                    echo ($lehrer_namen != null) ? '<span class="kalender-text"> - mit ' . $lehrer_namen . '</span></span>' : '</div>';
                                    echo ($location != null) ? '<p><span class="kalender-text"><span class="typcn typcn-location"></span>' . $location . '</span></p>' : '';
                                    echo ($price != null) ? '<p><span class="kalender-text"><span class="glyphicon glyphicon-euro"></span>' . $price . '</span></p>' : '';
                                    echo '<p class="kalender-text">' . $description . '</p>'
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                }else {
                    echo '<p>Zur Zeit gibt es keine laufenden Kurse.</p>';
                }
                ?>
            </div>

            <div id="jams" class="col-xs-12 col-md-10 col-md-offset-1">

                <h2>Jams</h2>
                <?php
                if (sizeof(get_events_jams()) != 0) {
                    foreach (get_events_jams() as $event) {

                        setlocale(LC_TIME, 'de_DE@euro', 'de_DE', 'de', 'ge');

                        $weekday = strftime('%a', strtotime($event->datetime_start));
                        $start_time = strftime('%H:%M', strtotime($event->datetime_start));
                        $end_time = strftime('%H:%M', strtotime($event->datetime_end));
                        $title = $event->titel;
                        $location = $event->ort;
                        $city = $event->city;
                        $price = $event->preis;

                        $events_id = $event->events_id;
                        $farbenarray = $wpdb->get_results(get_color_query() . " WHERE ${events_id} = wp_events_farben_relate.events_id" );

                        $count=0;
                        if (sizeof($farbenarray) == 1){
                            foreach ($farbenarray as $farbe) {
                                $farben_werte = $farbe->wert.', '.$farbe->wert;
                            }
                        }else{
                            foreach ($farbenarray as $farbe){
                                if ($count==0){
                                    $farben_werte = $farbe->wert;
                                }else{
                                    $farben_werte = $farben_werte.', '.$farbe->wert;
                                }
                                $count++;
                            }
                        }


                        ?>

                        <table class="kalender-table">
                            <tr>
                                <?php
                                echo ($city != 'hh' && $city != '') ? '<div class="kalender-place"><img src="'. $image_url .''. $city .'.png" alt="'. $city .'"></div>' : '';
                                ?>
                                <td class="table-date">
                                    <?php
                                    echo '<p class="kalender-tag">' . $weekday . '</p>';
                                    echo '<p class="kalender-jahr">' . $start_time . '</p>';
                                    echo '<p class="kalender-jahr">' . $end_time . '</p>';
                                    ?>
                                </td>
                                <td class="table-desc">
                                    <?php
                                    echo '<div style="float:left;"><div class="kalender-farbe" style="background-image: linear-gradient(to top, '.$farben_werte.');"></div></div><p class="kalender-title">' . $title . '</p>';
                                    echo ($location != null) ? '<p><span class="kalender-text"><span class="typcn typcn-location"></span>' . $location . '</span></p>' : '';
                                    echo ($price != null) ? '<p><span class="kalender-text"><span class="glyphicon glyphicon-euro"></span>' . $price . '</span></p>' : '';
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <?php
                    }
                }else {
                    echo '<p>Es gibt zur Zeit keine regelmäßigen Jams.</p>';
                }
                ?>
            </div>


            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <div class="text-center">
                    <a class="btn btn-custom" href="#orte" role="button">Mehr zu den Orten</a>
                </div>
                <table class="legende table-resp">
                    <tr class="tr-resp">
                        <td class="legende td-resp"><p class="kalender-title">Legende: </p></td>
                        <td class="legende td-resp"><div class="legende-farbe" style="background-color: #7d55c7;"></div></td>
                        <td class="legende td-resp"><p>Yoga</p></td>
                        <td class="legende td-resp"><div class="legende-farbe" style="background-color: #41b6e6;"></div></td>
                        <td class="legende td-resp"><p>Therapeutic</p></td>
                        <td class="legende td-resp"><div class="legende-farbe" style="background-color: #e03c31;"></div></td>
                        <td class="legende td-resp"><p>Solar</p></td>
                    </tr>
                </table>
            </div>

        </div


        </div>
        </div>

    </div>

<section class="container-parallax info03" data-stellar-background-ratio="0.5"></section>

<section id="orte">
    <div class="container">
        <h2>Orte</h2>
        <div class="row">
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/aspria_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/XxrsYtosryv" target="_blank" class="expand">Aspria Alstertal<br />Rehagen 20, 22339 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/aspria_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/DA69GiEMRY92" target="_blank" class="expand">Aspria Uhlenhorst<br />Hofweg 40, 22085 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/kaifu_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/183PFvTAASJ2" target="_blank" class="expand">Kaifu-Lodge<br />Bundesstraße 107, 20144 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/swdc_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/8N5pqfy9RWv" target="_blank" class="expand">Scheinwerfer DanceCenter<br />Am Veringhof 23b, 21107 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/flashh_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/zLyXQJKatuE2" target="_blank" class="expand">FLASHH<br />Gasstraße 18, 22761 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/abenteuer_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/b8MDG26Q8Uq" target="_blank" class="expand">Abenteuer Turnhalle<br />Wormser Str., 22761 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/sutra_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/qG2xzoytx3k" target="_blank" class="expand">Sutra Collective<br />Talstraße 19, 20359 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/hsp_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/RaUChERqVft" target="_blank" class="expand">Hochschulsport Hamburg<br />Turmweg 2,20148 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/turnhalle_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/gMmUcGYATKG2" target="_blank" class="expand">Turnhalle Hamburg<br />Osterbekstraße 90 b, 22083 Hamburg</a>
                    </div>
                </div>
            </div>
            <div class="places-item effects col-md-3">
                <div class="img places-item">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/assets/img/orte/mindspace_logo.jpg"/>
                    <div class="overlay">
                        <a href="https://goo.gl/maps/MBddLp8BbQL2" target="_blank" class="expand">Mindspace Hamburg<br />Rödingsmarkt 9,20459 Hamburg</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

</body>
</html>