// Lib Dependencies

require('./bootstrap');


//  Vue Components

Vue.component('calendar-root', require('./components/root-plugin-component.vue'));
Vue.component('edit-event-list', require('./components/edit-event-list.vue'));

Vue.component('add-event-input', require('./components/add-event-form.vue'));
Vue.component('datepicker', require('vuejs-datepicker'));

Vue.component('init-database', require('./components/initial-data-input.vue'));



Vue.config.devtools = true;

const app = new Vue({
    el: '#kalender-container'
});