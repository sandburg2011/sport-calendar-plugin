//  Require Styles

require('../style/main.scss');


//  Require JS Libs

window._ = require('underscore');

window.$ = window.jQuery = require('jquery');

window.Vue = require('vue/dist/vue.js');

window.moment = require('moment');
import 'moment/locale/de';
moment.locale('de');

require('bootstrap-select/js/bootstrap-select.js');
require('bootstrap-sass');

var jQueryBridget = require('jquery-bridget');
var Isotope = require('isotope-layout');
// make Isotope a jQuery plugin
jQueryBridget( 'isotope', Isotope, $ );